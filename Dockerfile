FROM node:16 as builder
WORKDIR /todomvc
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
RUN npm install
COPY . ./
RUN npm run build
#CMD [ "npm", "run", "dev" ]

FROM nginx:latest
COPY --from=builder /todomvc/dist/ /usr/share/nginx/html/



