// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  
  before(() => {
    cy.visit('/')
  })
    
  it('should add elements', () => {
    cy.get('.new-todo').type("test").type('{enter}')
    cy.get('.new-todo').type("test2").type('{enter}')
    cy.get('.new-todo').type("test3").type('{enter}')
    cy.get('.todo-count').should("contain", "3items left")
  })

  it('should check if element is active', () => {
    cy.get(':nth-child(1) > .view > label').should('be.visible')
    cy.get('.toggle').should('not.be.checked')
  })

  it('should check element', () => {
    cy.get(':nth-child(1) > .view > .toggle').click()
    cy.get('.todo-count').should("contain", "2items left")
  })

  it('should add element', () => {
    cy.get('.new-todo').type("test4").type('{enter}')
    cy.get('.todo-count').should("contain", "3items left")
  })

  it('should check and uncheck element', () => {
    cy.get(':nth-child(4) > .view > .toggle').click()
    cy.get('.todo-count').should("contain", "2items left")
    cy.get(':nth-child(4) > .view > .toggle').click()
    cy.get('.todo-count').should("contain", "3items left")
  })

  it('completed filter should work', () => {
    cy.get(':nth-child(2) > a').click()
    cy.get('li.todo').should("have.length",3)
  })

  it('active filter should work', () => {
    cy.get(':nth-child(3) > a').click()
    cy.get('li.todo').should("have.length",1)
  })

  it('clear completed should work', () => {
    cy.get('.clear-completed').click()
    cy.get('li.todo').should("have.length",0)
    cy.get('.todo-count').should("contain", "3items left")
  })
})
